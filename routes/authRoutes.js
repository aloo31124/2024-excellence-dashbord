const { Router } = require('express');
const authController = require('../controllers/authController');

// 2-1. 新增 路由路徑與 js檔, 使用 router實例, get,post 請求 與 controller 溝通
const router = Router();
router.get('/signup', authController.signup_get);
router.post('/signup', authController.signup_post);
router.get('/login', authController.login_get);
router.post('/login', authController.login_post);
router.get('/logout', authController.logout_get);

module.exports = router;