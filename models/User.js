const mongoose = require('mongoose');
const { isEmail } = require('validator');
const bcrypt = require('bcrypt');

/* 建立 使用者  User (mongoose) DB schema (type, require, length) */
// 4-1. 建立 Model > user => 用於 mongodb 溝通, 註冊 表, 定義 User之 db Schema (type, require, limit)
// 5-1. Model-user 中 required, validate(isEmail), minlength => 驗證檢查表單
const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, '請輸入信箱'],
    unique: true,
    lowercase: true,
    validate: [isEmail, '請輸入有效信箱']
  },
  password: {
    type: String,
    required: [true, '請輸入密碼'],
    minlength: [6, '密碼最少6個字元'],
  }
});


// fire a function before doc saved to db
userSchema.pre('save', async function(next) {
  const salt = await bcrypt.genSalt();
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

// static method to login user
userSchema.statics.login = async function(email, password) {
  const user = await this.findOne({ email });
  if (user) {
    const auth = await bcrypt.compare(password, user.password);
    if (auth) {
      return user;
    }
    throw Error('incorrect password');
  }
  throw Error('incorrect email');
};

/* 向 mongoose model 註冊 user 表 */
const User = mongoose.model('user', userSchema);
module.exports = User;