const User = require("../models/User");
const jwt = require('jsonwebtoken');

const handleErrors = (err) => {
  console.log(err.message, err.code);
  let errors = { email: '', password: '' };

  // 5-2. 於 auth controller 檢查 登入驗證
  if (err.message === 'incorrect email') {
    errors.email = '信箱錯誤,或無此信箱';
  }
  if (err.message === 'incorrect password') {
    errors.password = '密碼錯誤!';
  }
  if (err.code === 11000) {
    errors.email = '此信箱已被註冊';
    return errors;
  }


  // 5-3. signup.ejs 註冊頁, 於 auth controller > handleErrors 檢查
  if (err.message.includes('user validation failed')) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  }

  return errors;
}

// create json web token
const maxAge = 3 * 24 * 60 * 60;
const createToken = (id) => {
  return jwt.sign({ id }, 'net ninja secret', {
    expiresIn: maxAge
  });
};

// 2-2. 新增 controller 路徑與 js檔, 使用 moudle.export.* <fun名稱> => router.js 才可使用
module.exports.signup_get = (req, res) => {
  res.render('signup');
}

module.exports.login_get = (req, res) => {
  res.render('login');
}

/* 4-2. auth controller 使用 Model 操作db => User.create 創建使用者, 其 return promise, 需使用 [async/await]  */
module.exports.signup_post = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.create({ email, password });
    const token = createToken(user._id);
    res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
    res.status(201).json({ user: user._id });
  }
  catch(err) {
    // 5-4. signup.ejs 註冊頁 如何讓 auth controller 檢查? ans: 路由
    const errors = handleErrors(err);
    res.status(400).json({ errors });
  }
 
}

module.exports.login_post = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.login(email, password);
    const token = createToken(user._id);
    res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
    res.status(200).json({ user: user._id });
  } 
  catch (err) {
    const errors = handleErrors(err);
    res.status(400).json({ errors });
  }

}

module.exports.logout_get = (req, res) => {
  res.cookie('jwt', '', { maxAge: 1 });
  res.redirect('/');
}