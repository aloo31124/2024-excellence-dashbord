


==================================================================================
05/28
ch5 Node Auth Tutorial (JWT) #5 - Mongoose Validation ([登入/註冊]表單驗證檢查)
https://www.youtube.com/watch?v=nukNITdis9g&list=PL4cUxeGkcC9iqqESP8335DA5cRFp8loyp&index=5
=> 5-1. Model-user 中 required, validate(isEmail), minlength => 驗證檢查表單
=> 5-2. 於 auth controller 檢查 登入驗證
=> 5-3. signup.ejs 註冊頁, 於 auth controller > handleErrors 檢查
=> 5-4. signup.ejs 註冊頁 如何讓 auth controller 檢查? ans: 路由, signup.ejs (url+post) => auth Routes authController.signup_post => auth controller
=> 備註: 密碼尚未 雜湊加密


==================================================================================
05/28
ch4. Node Auth Tutorial (JWT) #4 - User Model
https://www.youtube.com/watch?v=mnJxyc0DGM8&list=PL4cUxeGkcC9iqqESP8335DA5cRFp8loyp&index=4
=> 4-1. 建立 Model-user => 用於 mongodb 溝通, 註冊 表, 定義 User之 db Schema (type, require, limit)
=> 4-2. auth controller 使用 Model 操作db => User.create 創建使用者, 其 return promise, 需使用 [async/await] 
=> 4-3. postman 測試 => 先略
=> 4-4. Mongo DB 檢視資料: 進入 project > (上方) Database sersvices > (內容頁) Browse Collections > (DB)test > (table)users


==================================================================================
Node Auth Tutorial (JWT) #3 - Testing Routes & Handling POST Requests
https://www.youtube.com/watch?v=uiKwHx2K1Fo&list=PL4cUxeGkcC9iqqESP8335DA5cRFp8loyp&index=3
=> 下載 post man
=> http://localhost:3000/ => 沒反應 ="=

5/27

==================================================================================
https://www.youtube.com/watch?v=muhJTRQ7WMk&list=PL4cUxeGkcC9iqqESP8335DA5cRFp8loyp&index=2
part 2, auth routes & controllers
    =>  2-1. 新增 路由路徑與 js檔, 使用 router實例, get,post 請求 與 controller 溝通
    =>  2-2. 新增 controller 路徑與 js檔, 使用 moudle.export.* <fun名稱> => router.js 才可使用
    =>  2-3. 新增 view/ login登入, signup註冊 兩 ejs 畫面




